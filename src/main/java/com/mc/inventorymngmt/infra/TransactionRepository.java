package com.mc.inventorymngmt.infra;

import com.mc.inventorymngmt.entity.Transaction;
import com.mc.inventorymngmt.entity.TransactionItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Transaction findByTransactionId(Long transactionId);

    @Query(value = "select count(*) from transaction where transaction_id = ?1 and merchant_id =?2 and correllation_id =?3",  nativeQuery = true)
    Integer isExistsWithRecordWithCombination(Long transactionId, Integer merchantId, Integer correlationId);
}
