package com.mc.inventorymngmt.domain;

import com.mc.inventorymngmt.dto.InventoryCreateDto;
import com.mc.inventorymngmt.dto.TransactionCreateDto;
import com.mc.inventorymngmt.entity.Inventory;
import com.mc.inventorymngmt.entity.Transaction;
import com.mc.inventorymngmt.entity.TransactionItems;
import com.mc.inventorymngmt.error.InvalidInventoryEntryException;
import com.mc.inventorymngmt.error.InvalidTransactionException;
import com.mc.inventorymngmt.error.InventoryNotFoundException;
import com.mc.inventorymngmt.infra.InventoryRepository;
import com.mc.inventorymngmt.infra.TransactionItemRepository;
import com.mc.inventorymngmt.infra.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class InventoryManagementDomainService {
    @Autowired
    private final TransactionRepository transactionRepository;
    private final InventoryRepository inventoryRepository;
    private final TransactionItemRepository transactionItemRepository;

    public InventoryManagementDomainService(TransactionRepository transactionRepository, InventoryRepository inventoryRepository, TransactionItemRepository transactionItemRepository) {
        this.transactionRepository = transactionRepository;
        this.inventoryRepository = inventoryRepository;
        this.transactionItemRepository = transactionItemRepository;
    }


    public List<Transaction> retrieveTransactions() {
        return transactionRepository.findAll();
    }

    public Transaction getTransaction(Long transactionId) {
        return transactionRepository.findByTransactionId(transactionId);
    }

    public void saveTransaction(TransactionCreateDto dto) throws InventoryNotFoundException, InvalidInventoryEntryException{

        //Map DTo to Entity Object
        if(transactionRepository.isExistsWithRecordWithCombination(
                dto.getTransactionId(),
                dto.getMerchantId(),
                dto.getCorrellationId())== 0){

            Set<Integer> duplicateItems = new HashSet<>();

            if(dto.getItemDtoList().stream()
                    .map(n->n.getId())
                    .filter(n -> !duplicateItems.add(n)) // Set.add() returns false if the element was already in the set.
                    .collect(Collectors.toSet()).size()>0){
                throw new InvalidTransactionException("Multiple entries of same Item Type found, please group them together");

            }else {

                Transaction transaction = new Transaction();
                transaction.setTransactionId(dto.getTransactionId());
                transaction.setMerchantId(dto.getMerchantId());
                transaction.setAmount(dto.getAmount());
                transaction.setCorrellationId(dto.getCorrellationId());
                transaction.setApiUser(dto.getApiUser());
                transaction.setApiPwd(dto.getApiPwd());

                transaction.setCardNumber(dto.getCardNumber());
                transaction.setCsv(dto.getCsv());
                transaction.setExpiryDate(dto.getExpiryDate());

                List<Inventory> inventoryList = inventoryRepository.findByMerchantId(
                        dto.getMerchantId()).orElseThrow(() -> new InventoryNotFoundException("Merchant Id not found in Inventory"));
                if (dto.getItemDtoList() != null) {
                    dto.getItemDtoList().forEach(itemDto -> {
                        //Check Inventory Stock
                        List<Inventory> matchedInventory = inventoryList
                                .stream()
                                .filter(inventory -> inventory.getId().equals(itemDto.getId())
                                        && inventory.getCount() >= itemDto.getQuantity()).collect(Collectors.toList());

                        //Update Inventory Stock
                        if (!matchedInventory.isEmpty()) {
                            //Deduct the quantity from Inventory
                            matchedInventory.get(0).setCount(matchedInventory.get(0).getCount() - itemDto.getQuantity());
                            inventoryRepository.save(matchedInventory.get(0));

                            //Save the Transaction Items
                            transactionItemRepository.save(new TransactionItems(itemDto.getId(), dto.getTransactionId(), itemDto.getQuantity()));
                        } else {
                            throw new InventoryNotFoundException("Item not found in Inventory or insufficient quantity");
                        }
                    });
                }

                //Save the Transaction

                transactionRepository.save(transaction);
            }
        } else{
            throw new InvalidTransactionException("There exists a transaction with similar combination of Transaction , Merchant and correlation");
        }

    }

    public void createInventory(Integer merchantId, InventoryCreateDto dto) throws InvalidInventoryEntryException{

        //Create New Stock in the Inventory

        if (inventoryRepository.isExistsWithRecordWithCombination(dto.getItemId(), merchantId) == 0) {
            Inventory inventory = new Inventory(dto.getItemId(), merchantId, dto.getCount());
            inventoryRepository.save(inventory);
        }
        else{
            throw new InvalidInventoryEntryException("There exists an Inventory with similar combination of merchant and Item id");
        }



    }

}
