package com.mc.inventorymngmt.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class ErrorAdvice {

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<String> handleRunTimeException(RuntimeException e) {
        return error(INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler({InvalidInventoryEntryException.class})
    public ResponseEntity<String> handleNotFoundException(InvalidInventoryEntryException e) {
        return error(NOT_ACCEPTABLE, e);
    }

    @ExceptionHandler({InvalidTransactionException.class})
    public ResponseEntity<String> handleDogsServiceException(InvalidTransactionException e) {
        return error(NOT_ACCEPTABLE, e);
    }

    @ExceptionHandler({InventoryNotFoundException.class})
    public ResponseEntity<String> handleDogsServiceException(InventoryNotFoundException e) {
        return error(NOT_FOUND, e);
    }

    private ResponseEntity<String> error(HttpStatus status, Exception e) {
        return ResponseEntity.status(status).body(e.getMessage());
    }
}
